package com.kozlov.app;

import javax.swing.*;
import java.awt.*;

public class JCurvesPaintingSheet extends JPanel {
    private JFrame parentFrame;
    private int width;
    private int height;
    private int xBound;
    private int yBound;
    private Color bgColor;
    private DrawableSylizedCurves content;

    public JCurvesPaintingSheet(JFrame parentFrame, DrawableSylizedCurves content, int width, int height, int xBound, int yBound, Color bgColor) {
        this.parentFrame = parentFrame;
        this.content     = content;
        this.width       = width;
        this.height      = height;
        this.xBound      = xBound;
        this.yBound      = yBound;
        this.bgColor     = bgColor;

        initForm();
    }

    void initForm() {
        this.setBounds(xBound, yBound, width, height);
        this.setBackground(bgColor);
        parentFrame.add(this);
    }

    public void setParentFrame(JFrame parentFrame) {
        parentFrame.remove(this);
        this.parentFrame = parentFrame;
        parentFrame.add(this);
    }

    public void setBgColor(Color bgColor) {
        parentFrame.remove(this);
        this.bgColor = bgColor;
        parentFrame.add(this);
    }

    public void setHeight(int height) {
        parentFrame.remove(this);
        this.height = height;
        parentFrame.add(this);
    }

    public void setWidth(int width) {
        parentFrame.remove(this);
        this.width = width;
        parentFrame.add(this);
    }

    public void setxBound(int xBound) {
        parentFrame.remove(this);
        this.xBound = xBound;
        parentFrame.add(this);
    }

    public void setyBound(int yBound) {
        parentFrame.remove(this);
        this.yBound = yBound;
        parentFrame.add(this);
    }

    public Color getBgColor() {
        return bgColor;
    }

    public DrawableSylizedCurves getContent() {
        return content;
    }

    public int getxBound() {
        return xBound;
    }

    public int getyBound() {
        return yBound;
    }

    public JFrame getParentFrame() {
        return parentFrame;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        content.drawContent(g);
    }
}
