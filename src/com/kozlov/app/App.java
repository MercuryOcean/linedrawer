package com.kozlov.app;

import com.kozlov.geometry.Bezier;
import com.kozlov.geometry.Point;
import com.kozlov.visual.CurveStyleName;
import com.kozlov.visual.VisualBezier;
import com.kozlov.visual.VisualCurve;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Random;

public class App extends JPanel{
    CurvesSVGExporter exporter;
    JCurvesPaintingSheet paintingSheet;
    DrawableSylizedCurves curves;
    JFileChooser fileChooser;

    public App(){
        JFrame f = new JFrame("Draw Line");
        exporter = new CurvesSVGExporter();
        curves = new DrawableSylizedCurves();
        paintingSheet = new JCurvesPaintingSheet(f, curves,575, 495, 5, 5, Color.white);
        JFileChooser fileChooser = new JFileChooser();

        fileChooser.setDialogTitle("Specify a file to save");
        JButton generateButton = new JButton("Generate");
        generateButton.setBounds(20,520,95,30);
        f.add(generateButton);

        JButton saveButton = new JButton("Save");
        saveButton.setBounds(generateButton.getWidth() + generateButton.getX(),520,95,30);
        f.add(saveButton);

        JRadioButton greenRadio = new JRadioButton("Green", true);
        JRadioButton dottedRadio = new JRadioButton("Dotted");
        greenRadio.setBounds( 20 + generateButton.getX() + generateButton.getWidth() + saveButton.getWidth(), 520, 60, 30);
        dottedRadio.setBounds( 20 + generateButton.getX() + generateButton.getWidth() + saveButton.getWidth() + greenRadio.getWidth(), 520, 95, 30);
        ButtonGroup bgroup = new ButtonGroup();
        bgroup.add(greenRadio);
        bgroup.add(dottedRadio);
        f.add(greenRadio);
        f.add(dottedRadio);

        generateButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                curves.put(getRandomCurve(), greenRadio.isSelected() ? CurveStyleName.GREEN_ARROWED :
                        CurveStyleName.BLACK_DOTTED);
                paintingSheet.repaint();
            }
        });

        saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int userSelection = fileChooser.showSaveDialog(f);

                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    curves.drawContent(exporter.getGraphicContext(), greenRadio.isSelected() ? CurveStyleName.GREEN_ARROWED :
                                                                                               CurveStyleName.BLACK_DOTTED);
                    exporter.export(fileToSave);
                };
            }
        });

        GraphicLineSample gls = new GraphicLineSample();
        f.add(gls);

        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(600, 600);
        f.setLayout(null);
        f.setVisible(true);
    }


    protected VisualCurve getRandomCurve() {
        // Random for X
        int minX = 0;
        int maxX = paintingSheet.getHeight();
        int diffX = maxX- minX;
        double[] xArr = new double[4];
        for (int i = 0; i < 4; i++) {
            Random random = new Random();
            int x = random.nextInt(diffX + 1);
            x += minX;
            xArr[i] = x;
        }

        // Random for Y
        int minY = 0;
        int maxY = paintingSheet.getWidth();
        int diffY = maxY- minY;
        double[] yArr = new double[4];
        for (int i = 0; i < 4; i++) {
            Random random = new Random();
            int y = random.nextInt(diffY + 1);
            y += minY;
            yArr[i] = y;
        }

        VisualCurve curve = new VisualBezier(new Bezier(new com.kozlov.geometry.Point(xArr[0], yArr[0]),
                new com.kozlov.geometry.Point(xArr[1], yArr[1]),
                new com.kozlov.geometry.Point(xArr[2], yArr[2]),
                new Point(xArr[3], yArr[3])));

        return curve;
    }
}
