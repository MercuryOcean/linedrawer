package com.kozlov.app;

import com.kozlov.geometry.Bezier;
import com.kozlov.geometry.Line;
import com.kozlov.geometry.Point;
import com.kozlov.visual.*;

import java.awt.*;

public class GraphicLineSample extends Canvas {
    public void paint(Graphics g) {
        VisualCurve curveLine = new VisualLine(new Line(new com.kozlov.geometry.Point(125,250), new com.kozlov.geometry.Point(500,250)));


        g.setColor(Color.blue);
        VisualCurve curveBizier = new VisualBezier(new Bezier(new com.kozlov.geometry.Point(0, 0), new com.kozlov.geometry.Point(500, 500), new com.kozlov.geometry.Point(1000, 0), new Point(500, 1000)));

        ICurveStyle dotted = new DottedCurveStyle();
        dotted.stylize(curveBizier).draw(g);

        ICurveStyle green = new GreenCurveStyle();
        green.stylize(curveLine).draw(g);
    }
}
