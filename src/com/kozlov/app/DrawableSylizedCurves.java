package com.kozlov.app;

import com.kozlov.visual.CurveStyleName;
import com.kozlov.visual.CurveStylizer;
import com.kozlov.visual.VisualCurve;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class DrawableSylizedCurves {
    Map<VisualCurve, CurveStyleName> content;

    public DrawableSylizedCurves() {
        content = new HashMap<>();
    }

    public CurveStyleName getStyleName(VisualCurve visualCurve) {
        return content.get(visualCurve);
    }

    public void put(VisualCurve visualCurve, CurveStyleName curveStyleName) {
        content.put(visualCurve, curveStyleName);
    }

    public boolean isEmpty() {
        return content.isEmpty();
    }

    public void drawContent(Graphics g) {
        for (VisualCurve curve : content.keySet()) {
             CurveStylizer.getStylized(curve, getStyleName(curve)).draw(g);
        }
    }

    public void drawContent(Graphics g, CurveStyleName styleName) {
        for (VisualCurve curve : content.keySet()) {
            if (getStyleName(curve) == styleName) {
                CurveStylizer.getStylized(curve, styleName).draw(g);
            }
        }
    }
}
