package com.kozlov.app;

import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.svg.SVGDocument;

import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

public class CurvesSVGExporter {
    protected SVGGraphics2D graphics;
    protected SVGDocument doc;

    public CurvesSVGExporter() {
        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
        doc = (SVGDocument) impl.createDocument(svgNS, "svg", null);
        graphics = new SVGGraphics2D(doc);
    }

    public void export(File file) {
        try {
            boolean useCSS = true;
//            Writer out = new OutputStreamWriter(System.out, "UTF-8");
            Writer out = new FileWriter(file);
            graphics.stream(out, useCSS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Graphics getGraphicContext() {
        return graphics;
    }
}
