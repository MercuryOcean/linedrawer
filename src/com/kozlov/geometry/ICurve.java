package com.kozlov.geometry;

public interface ICurve {
    IPoint getPoint(double t);
}
