package com.kozlov.geometry;

public abstract class CorrectCurve extends ACurve {
    CorrectCurve(IPoint a, IPoint b) {
        super(a,b);
    }

    public IPoint pointFromPoly(double t, IPoint... points) throws ArithmeticException{
        if (!isCorrectValue(t)) {
            throw new ArithmeticException("t /in [0,1]");
        }

        double[] pX = new double[points.length];
        double[] pY = new double[points.length];

        double resX = 0;
        double resY = 0;

        CurvePolynom curvePolynom = new CurvePolynom(t);

        for (int i = 0; i < points.length; i++) {
            pX[i] = points[i].getX();
            pY[i] = points[i].getY();
        }

        resX += curvePolynom.solve(pX);
        resY += curvePolynom.solve(pY);

        return new Point(resX, resY);
    }

    protected boolean isCorrectValue(double t) {
        if (0 <= t && t <= 1) {
            return true;
        }
        return false;
    }
}
