package com.kozlov.geometry;

public class Bezier extends CorrectCurve {
    protected IPoint c,d;

    public Bezier(IPoint a, IPoint b, IPoint c, IPoint d) {
        super(a, b);
        this.c = c;
        this.d = d;
    }

    @Override
    public IPoint getPoint(double t) throws ArithmeticException {
        try {
            return super.pointFromPoly(t, super.a, c, d, super.b);
        } catch (ArithmeticException ex) {
            throw ex;
        }
    }
}
