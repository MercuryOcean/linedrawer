package com.kozlov.geometry;


public class CurvePolynom {
    private double t;

    public CurvePolynom(double t){
        this.t = t;
    }

    public double solve(double... factors) {
        int power = factors.length - 1;
        double res = 0;
        for (int i = 0; i <= power; i++ ) {
            res += factors[i] * Math.pow((1 - t), power - i) * Math.pow(t, i);
        }
        return res;
    }
}
