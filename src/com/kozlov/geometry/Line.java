package com.kozlov.geometry;

public class Line extends CorrectCurve  {
    public  Line(IPoint a, IPoint b) {
        super(a, b);
    }

    @Override
    public IPoint getPoint(double t) throws ArithmeticException {
        try {
            return super.pointFromPoly(t, super.a, super.b);
        } catch (ArithmeticException ex) {
            throw ex;
        }
    }
}
