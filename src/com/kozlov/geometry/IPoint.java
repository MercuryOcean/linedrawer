package com.kozlov.geometry;

public interface IPoint {
    public double getX();
    public double getY();
    public void setX(double x);
    public void setY(double y);
}
