package com.kozlov.visual;

import com.kozlov.geometry.IPoint;

import java.awt.*;

public class DottedCurveStyle extends ACurveStyle{
    @Override
    protected void drawStyledStartPoint(Graphics g, IPoint startPoint, IPoint nearest) {
        g.setColor(Color.black);
        g.fill3DRect((int)Math.round(startPoint.getX()), (int)Math.round(startPoint.getY()), 10, 10, false);
    }

    @Override
    protected void setContentStyle(Graphics g) {
        g.setColor(Color.black);
        Graphics2D g2 = (Graphics2D)g;
        Stroke dashed = new BasicStroke(1, BasicStroke.CAP_SQUARE , BasicStroke.JOIN_BEVEL, 0, new float[]{1, 9}, 0);
        g2.setStroke(dashed);
    }

    @Override
    protected void drawStyledEndPoint(Graphics g, IPoint endPoint, IPoint nearest) {
        g.setColor(Color.black);
        g.fill3DRect((int)Math.round(endPoint.getX()), (int)Math.round(endPoint.getY()), 10, 10, false);
    }
}
