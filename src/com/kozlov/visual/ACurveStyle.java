package com.kozlov.visual;

import com.kozlov.geometry.IPoint;

import java.awt.*;

public abstract class ACurveStyle implements ICurveStyle {
    @Override
    public VisualCurve stylize(VisualCurve visualCurve) {
        return new VisualCurve(visualCurve.curve) {
            @Override
            protected void drawStartPoint(Graphics g) {
                drawStyledStartPoint(g, curve.getPoint(0), curve.getPoint(0.99));
            }

            @Override
            protected void drawContent(Graphics g) {
                setContentStyle(g);
                visualCurve.drawContent(g);
            }

            @Override
            protected void drawEndPoint(Graphics g) {
                drawStyledEndPoint(g, curve.getPoint(1), curve.getPoint(0.99));
            }
        };
    }

    protected abstract void drawStyledStartPoint(Graphics g, IPoint startPoint, IPoint nearest);
    protected abstract void setContentStyle(Graphics g);
    protected abstract void drawStyledEndPoint(Graphics g, IPoint endPoint, IPoint nearest);
}
