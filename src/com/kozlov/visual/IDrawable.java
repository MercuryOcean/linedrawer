package com.kozlov.visual;

import java.awt.*;

public interface IDrawable {
    void draw(Graphics g);
}
