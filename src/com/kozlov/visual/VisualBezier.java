package com.kozlov.visual;

import com.kozlov.geometry.*;
import java.awt.*;

public class VisualBezier extends VisualCurve {

    private final double parts = 100;

    public VisualBezier(Bezier bezier) {
        super(bezier);
    }

    @Override
    protected void drawContent(Graphics g) {
        for (int i = 0; i < parts ; i++) {
            int x1 =  (int)Math.round(curve.getPoint(1/parts * i).getX());
            int y1 =  (int)Math.round(curve.getPoint(1/parts * i).getY());

            int x2 =  (int)Math.round(curve.getPoint(1/parts * (i+1)).getX());
            int y2 =  (int)Math.round(curve.getPoint(1/parts * (i+1)).getY());

            g.drawLine(x1, y1, x2, y2);
        }
    }
}
