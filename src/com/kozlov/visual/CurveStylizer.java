package com.kozlov.visual;

public class CurveStylizer {
    static public VisualCurve getStylized(VisualCurve visualCurve, CurveStyleName style) {
        ICurveStyle stylizeEngine = new DefaultCurveStyle();
        if (style == CurveStyleName.GREEN_ARROWED) {
            stylizeEngine = new GreenCurveStyle();
        } else if (style == CurveStyleName.BLACK_DOTTED) {
            stylizeEngine = new DottedCurveStyle();
        }
        return stylizeEngine.stylize(visualCurve);
    }
}
