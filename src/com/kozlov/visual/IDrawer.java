package com.kozlov.visual;

public interface IDrawer {
    void draw(IDrawable drawable);
}
