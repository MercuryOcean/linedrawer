package com.kozlov.visual;

import com.kozlov.geometry.*;
import java.awt.*;

public abstract class VisualCurve implements IDrawable {
    protected ICurve curve;

    public VisualCurve(ICurve curve) {
        this.curve = curve;
    }

    @Override
    public void draw(Graphics g) {
        drawStartPoint(g);
        drawContent(g);
        drawEndPoint(g);
    }

    protected void drawStartPoint(Graphics g){
    }

    protected void drawContent(Graphics g) {
    }

    protected void drawEndPoint(Graphics g){
    }
}
