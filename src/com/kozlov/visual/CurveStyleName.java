package com.kozlov.visual;

public enum CurveStyleName {
    DEFAULT,
    GREEN_ARROWED,
    BLACK_DOTTED
}
