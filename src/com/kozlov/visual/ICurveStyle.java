package com.kozlov.visual;

public interface ICurveStyle {
    VisualCurve stylize(VisualCurve visualCurve);
}
