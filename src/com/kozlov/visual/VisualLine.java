package com.kozlov.visual;

import com.kozlov.geometry.*;
import java.awt.*;

public class VisualLine extends VisualCurve{
    public VisualLine(Line line) {
        super(line);
    }

    @Override
    protected void drawContent(Graphics g) {
        int x1 =  (int)Math.round(curve.getPoint(0).getX());
        int y1 =  (int)Math.round(curve.getPoint(0).getY());

        int x2 =  (int)Math.round(curve.getPoint(1).getX());
        int y2 =  (int)Math.round(curve.getPoint(1).getY());

        g.drawLine(x1, y1, x2, y2);
    }
}
