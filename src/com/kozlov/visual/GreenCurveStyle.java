package com.kozlov.visual;

import com.kozlov.geometry.IPoint;
import com.kozlov.geometry.Line;
import com.kozlov.geometry.Point;

import java.awt.*;

public class GreenCurveStyle extends ACurveStyle {
    private final static int radius = 5;

    @Override
    protected void drawStyledStartPoint(Graphics g, IPoint startPoint, IPoint nearest) {
        g.setColor(Color.green);
        g.fillOval((int)Math.round(startPoint.getX()) - radius, (int)Math.round(startPoint.getY()) - radius, radius*2, radius*2);
    }

    @Override
    protected void setContentStyle(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        Stroke dashed = new BasicStroke();
        g2.setStroke(dashed);
    }

    @Override
    protected void drawStyledEndPoint(Graphics g, IPoint endPoint, IPoint nearest) {
        double x = nearest.getX();
        double y = nearest.getY();

        double x1 = endPoint.getX();
        double y1 = endPoint.getY();

        double beta = Math.atan2(y-y1,x1-x);
        double alfa = Math.PI/30;
        int r1 = 20;

        int x2 =(int)Math.round(x1 - r1*Math.cos(beta + alfa));
        int y2 =(int)Math.round(y1 + r1*Math.sin(beta + alfa));
        g.drawLine((int)Math.round(x1),(int)Math.round(y1),x2,y2);

        x2 =(int)Math.round(x1 - r1*Math.cos(beta - alfa));
        y2 =(int)Math.round(y1 + r1*Math.sin(beta - alfa));
        g.drawLine((int)Math.round(x1),(int)Math.round(y1),x2,y2);
    }
}
