package com.kozlov.visual;

import com.kozlov.geometry.IPoint;

import java.awt.*;

public class DefaultCurveStyle extends ACurveStyle{
    @Override
    protected void drawStyledStartPoint(Graphics g, IPoint startPoint, IPoint nearest) {
        g.setColor(Color.black);
    }

    @Override
    protected void setContentStyle(Graphics g) {
        g.setColor(Color.black);
    }

    @Override
    protected void drawStyledEndPoint(Graphics g, IPoint endPoint, IPoint nearest) {
        g.setColor(Color.black);
    }
}
